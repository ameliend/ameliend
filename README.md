<p align="center">
  <img src="https://gitlab.com/ameliend/ameliend/-/raw/main/resources/2542850792.png">
</p>

# Hi 👋

I'm Amélien, Core Dev at [Fortiche](https://www.forticheprod.com/).

## Who the hell is [Nanachi](https://madeinabyssuniverse.fandom.com/fr/wiki/Nanachi) (My profile picture)?

As a Made in Abyss fan, I've adored Nanachi ever since she first appeared.
She was a person, who survived the Abyss curse, from the 6th level, and was 
transformed as a consequence. 
Nanachi has lived through terrible horrors, but has always retained a sane mind 
unlike most of the others.
Most of the time, Nanachi is reserved, keeping his distance emotionally and physically, 
but capable of deep compassion and selfless devotion. 
[Made in Abyss](https://www.nautiljon.com/animes/made+in+abyss.html) is known for his 
his very dark side, despite his childlike style.

## My artistic preferences

<p align="center">
  <img src="https://gitlab.com/ameliend/ameliend/-/raw/main/resources/eyes.png">
</p>

I'm fascinated by Japanese artistic aesthetics, particularly the anime style featuring 
creepy-looking female characters. I particularly like characters, often in pastel or
desaturated colors with disproportionate eyes. 

I'm captivated when an artist manages to create an atmosphere that's both 
unsettling but captivating, the way an artist can give the appearance of fragility in 
characters, while suggesting that they are capable of absolute cruelty, all without 
obvious expressions.

The eyes of a character are of the utmost importance to me, as they need to be both 
frightening and emotional.

## My personal projects

<p align="center">
  <img src="https://gitlab.com/ameliend/ameliend/-/raw/main/resources/banner.png">
</p>

I've created a whole universe in [VRChat](https://hello.vrchat.com/) 
featuring characters with their own stories.
The main character is called [Nekosan](https://scp-sandbox-3.wikidot.com/nekosan), 
a small character with pale skin, and white or slightly pink hair. Its eyes are red and 
disproportionately large in relation to its body. 

I've created 3 worlds and a total of 48 public avatars so far using Blender, 
Substance Painter, and Unity.

<p align="center">
  <img src="https://gitlab.com/ameliend/ameliend/-/raw/main/resources/worlds.png">
</p>

<img src="https://gitlab.com/ameliend/ameliend/-/raw/main/resources/2023-10-16%2018_48_42-Steam.png">

### Dev

The [VRChat Speech Assistant](https://gitlab.com/ameliend/vrchat-speech-assistant) 
allows you to use a speech to speech, with automatic translation over 30+ languages.
And / or speech to text with the VRChat chatbox using 
[python-osc](https://pypi.org/project/python-osc/).

[LolaBeta](https://gitlab.com/ameliend/LolaBeta), a Chatbot that can interact with
other people in VRChat.

## I ❤ Documentation

Code with clear function and variable names is always better, that's for sure.

```python
def add_heart_emoji_to_string(string):
    return f"{string} ❤"
```
But sometimes, if someone's idea seems understandable at the time, it may not be for 
everyone, or even for us, 3 years later.

```python
def zzz_calculation(input_arg):
    result = sum([item * 2 for item in input_arg if item % 2 == 0])
    temp_dict = {index: item for index, item in enumerate(input_arg) if item > 10}
    sorted_dict = dict(sorted(temp_dict.items(), key=lambda x: x[1], reverse=True))
    final_result = [value * key for key, value in sorted_dict.items()]
    return sum(final_result)
```

It starts with docstrings.

```python
def zzz_calculation(input_arg):
    """Perform a series of complex operations needed for the zzz machine.

    Args:
        input_arg (list): The list of numeric values that needs to be converted.

    Returns:
        int: The final sum of the processed elements in the input list.
    """
    result = sum([item * 2 for item in input_arg if item % 2 == 0])
    temp_dict = {index: item for index, item in enumerate(input_arg) if item > 10}
    sorted_dict = dict(sorted(temp_dict.items(), key=lambda x: x[1], reverse=True))
    final_result = [value * key for key, value in sorted_dict.items()]
    return sum(final_result)
```

Better 😀

```python
def zzz_calculation(input_arg: list) -> int:
    """Perform a series of complex operations needed for the zzz machine.

    Args:
        input_arg (list): The list of numeric values that needs to be converted.

    Returns:
        int: The final sum of the processed elements in the input list.

    Example:
    >>> input_list = [7, 12, 9, 18, 3, 15, 14]
    >>> zzz_calculation(input_list)
    82

    Note :
    The function name, "zzz_calculation" is cryptic and gives no indication of the 
    function's purpose, we recommend a more descriptive name.
    """
    result = sum([item * 2 for item in input_arg if item % 2 == 0])
    temp_dict = {index: item for index, item in enumerate(input_arg) if item > 10}
    sorted_dict = dict(sorted(temp_dict.items(), key=lambda x: x[1], reverse=True))
    final_result = [value * key for key, value in sorted_dict.items()]
    return sum(final_result)
```

Clear, detailed documentation enables new members to quickly understand the existing 
code, how it works and its purpose.

## Notable Aspects

* I Like cats, owning two.
* I enjoy Monster Energy drinks.
* I prefers Coca-Cola over water for hydration.
* I have a collection of over a hundred cans.
* I aspire to have twins.
* I can consume a kilo of cherry tomatoes in a day.
* I lack skill to make pancakes.
